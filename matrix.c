/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: matrix.c
*/

#include <stdlib.h>
#include "matrix.h"
#include "display.h"

#include <stdio.h>

/*Création d'une matrice avec les allocations nécessaires*/
/*@requires: un entier > 0
  @assigns: rien
  @ensures: retourne une matrice de taille n*/
matrix makeMatrix(int n) {
    matrix res;
    int i, j;

    res.size = n;
    res.hauteur_maxi = 0;

    res.t = calloc(n, sizeof(list*));
    /*Si l'allocation échoue (n trop grand), message d'erreur et on quitte*/
    if(!(res.t)) {
        fail_allocation();
        exit(0);
    }

    for(i = 0; i < n; i++) {
        res.t[i] = calloc(n, sizeof(list));
        /*Si l'allocation échoue (n trop grand), message d'erreur et on quitte*/
        if(!(res.t[i])) {
            fail_allocation();
            exit(0);
        }
        for(j = 0; j < n; j++)
            res.t[i][j] = NULL;
    }

    return res;
}

/*Libère la mémoire allouée pour une matrice*/
/*@requires: une matrice bien assignée
  @assigns: m
  @ensures: supprime m.t de la mémoire*/
void freeMatrix(matrix m) {
    int i, j;

    for(i = 0; i < m.size; i++) {
        for(j = 0; j < m.size; j++)
            make_empty(&(m.t[i][j]));
        free(m.t[i]);
    }
    free(m.t);
}

/*Met à joueur la hauteur maximale d'une matrice*/
/*@requires: une matrice bien assignée
  @assigns: gameboard
  @ensures: gameboard.hauteur_maxi est mise à jour*/
void update_hauteur(matrix *gameboard) {
    int i, j;
    int h_max = 0;

    for(i = 0; i < gameboard->size; i++)
        for(j = 0; j < gameboard->size; j++)
            if(hauteur(gameboard->t[i][j]) > h_max)
                h_max = hauteur(gameboard->t[i][j]);

    gameboard->hauteur_maxi = h_max;
}

/*Ajout d'un élement à une liste d'une matrice à une position donnée*/
/*@requires: une matrice bien assignée
  @assigns: gameboard.t
  @ensures: ajoute value à la liste m.t[y][x]*/
void add_matrix(matrix m, int x, int y, int value) {
    add(value, &(m.t[y][x]));
}

/*Suppression de l'élément en tête d'une liste d'une matrice à une position donnée*/
/*@requires: une matrice bien assignée
  @assigns: gameboard.t
  @ensures: supprime la valeur en tête de la liste m.t[y][x] et la retourne
            ou retourne 0 si la liste est vide*/
int pop_matrix(matrix m, int x, int y) {
    return pop(&(m.t[y][x]));
}
