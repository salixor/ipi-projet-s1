# Manuel d'utilisation du Morpion 3D

**Auteur :** Kévin Cocchi

**Groupe :** 3.2

**Fichiers :**
```
display.c, lists.c, main.c, matrix.c, victory.c
display.h, lists.h, main.h, matrix.h, victory.h
rapport.pdf
Makefile
README.md
```

**Prérequis :** Compilateur gcc, Bibliothèques standard C, Emulateur de terminal Unix, GNU Make (Optionnel)

*Il est recommandé d'utiliser un terminal supportant l'affichage des couleurs pour lancer le programme.*

## 1 - Extraction des fichiers sources

L'archive contient l'ensemble des fichiers sources nécessaires à la bonne compilation du programme, le rapport de projet, ce manuel d'utilisation, ainsi qu'un Makefile doté des options ```-Wall -Wextra -ansi``` comme requis par l'énoncé.

Placez l'archive dans le dossier de votre choix et extrayez les fichiers en utilisant la commande :

```
tar -xf kevin_cocchi.tgz
```

## 2 - Compilation

La compilation du programme s'effectue ainsi dans le dossier du projet :

- Si GNU Make est installé :
`make morpion`

- Si GNU Make n'est pas installé :
```
gcc -Wall -Wextra -ansi -c lists.c
gcc -Wall -Wextra -ansi -c matrix.c
gcc -Wall -Wextra -ansi -c victory.c
gcc -Wall -Wextra -ansi -c display.c
gcc -Wall -Wextra -ansi -c main.c -lm

gcc -Wall -Wextra -ansi lists.o matrix.o victory.o display.o main.o -o morpion -lm
```

## 3 - Exécution du programme

Une fois compilé, il est possible de lancer le morpion en se plaçant dans le dossier du projet (ou le dossier où a été compilé le programme) et en utilisant la commande :

- Sur un système UNIX : `./morpion <taille du plateau>=[3,4,...] <mode>={2,3} <seisme>={y,n}`
- Sur un système WINDOWS : `./morpion.exe <taille du plateau>=[3,4,...] <mode>={2,3} <seisme>={y,n}`
