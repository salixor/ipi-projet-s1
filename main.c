/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: main.c
*/

#include "lists.h"
#include "matrix.h"
#include "victory.h"
#include "display.h"
#include "main.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <time.h> /*Pour changer la seed dans srand()*/
#include <ctype.h> /*Pour utiliser tolower*/
#include <math.h> /*Pour utiliser pow*/

char TOUCHES[8] = {'i', 'j', 'k', 'l', 'p', 'r', 'q', 'a'};
char JETONS[2] = {'O', 'X'};


int main(int argc, char* argv[]) {
    /*Variables pour le déroulement du jeu*/
    int victoire = -1;
    int game_ended = 0; /*Partie finie*/
    int game_continue = 1; /*Jeu en cours*/
    int game_reset = 0; /*Pour les nouvelles parties*/

    /*Paramètres de la partie*/
    int seisme = 0;
    int mode = 0;

    int player = 1; /*Variable indiquant le joueur actuel*/

    /*Variables utilisées pour le curseur*/
    int x = 0;
    int y = 0;

    /*Variables pour conserver une trace des erreurs d'action du joueur*/
    int depile_impossible = 0;
    int deplacement_impossible = 0;
    int action_invalide = 0;

    int **cases_seisme = NULL; /*Tableau 2D pour les cases où il y a séisme*/

    /*Variables pour les différents input de l'utilisateur*/
    char user_input[10];
    int size_inp = 0;
    char mode_inp = ' ';
    char seisme_inp = ' ';
    char c = ' ';

    int i, j, k;

    srand(time(NULL)); /*Nouvelle seed pour l'aléatoire*/



    /*-----------------------------------------------------------------------*/
    /*            CE TRONCON N'EST EXECUTE QU'AU LANCEMENT DU JEU            */
    /*-----------------------------------------------------------------------*/

    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

    /*AFFICHAGE D'UN MESSAGE DE BIENVENUE*/
    printf(ANSI_COLOR_RED);
    printf("   === BIENVENUE AU JEU DU MORPION 3D ===");
    printf(ANSI_COLOR_RESET);
    printf("\n\n");


    /*PARTIE 1 : DEFINITION DE LA TAILLE DU PLATEAU DE JEU*/

    /*On peut passer la taille du plateau en argument ...*/
    if(argc >= 2)
        size_inp = (int) strtol(argv[1], NULL, 10);

    /*... ou la rentrer directement dans le programme*/
    /*Cette boucle a lieu tant que l'input de l'utilisateur ne convient pas*/
    while(size_inp < 3) {
        printf("Entrez la taille du tableau (3 minimum) : ");
        fflush(stdout);
        scanf("%s", user_input);
        size_inp = (int) strtol(user_input, NULL, 10);
    }

    matrix gameboard = makeMatrix(size_inp); /* Création de la matrice */


    /*PARTIE 2 : CHOIX DU MODE 2D OU 3D*/

    /*On peut choisir le mode en argument ...*/
    if(argc >= 3)
        mode_inp = tolower(argv[2][0]);

    /*... ou le choisir directement dans le programme*/
    /*Cette boucle a lieu tant que l'input de l'utilisateur ne convient pas*/
    while(mode_inp != '2' && mode_inp != '3') {
        printf("Souhaitez-vous jouer en 2D ou en 3D ? (2/3) : ");
        fflush(stdout);
        scanf("%s", user_input);
        mode_inp = tolower(user_input[0]);
    }

    /*On met à jour la variable qui indique le mode choisi*/
    if(mode_inp == '2')  mode = 2;
    if(mode_inp == '3')  mode = 3;

    /*On change la fonction de vérification de victoire selon le mode choisi*/
    if(mode == 2)        check_victoire = &check_victoire_2D;
    if(mode == 3)        check_victoire = &check_victoire_3D;


    /*PARTIE 3 : CHOIX DU MODE SEISME OU NON*/

    /*On peut choisir le mode seisme en argument ...*/
    if(argc >= 4)
        seisme_inp = tolower(argv[3][0]);

    /*... ou le choisir directement dans le programme*/
    /*Cette boucle a lieu tant que l'input de l'utilisateur ne convient pas*/
    while(seisme_inp != 'y' && seisme_inp != 'n') {
        printf("Souhaitez-vous activer le mode séisme ? (y/n) : ");
        fflush(stdout);
        scanf("%s", user_input);
        seisme_inp = tolower(user_input[0]);
    }

    /*On met à jour la variable qui indique le mode choisi*/
    if(seisme_inp == 'y')        seisme = 1;
    else if(seisme_inp == 'n')   seisme = 0;

    /*Si le mode séisme est actif, on alloue de la mémoire pour le tableau*/
    if(seisme) {
        cases_seisme = calloc(gameboard.size, sizeof(int*));
        for(i = 0; i < gameboard.size; i++)
            cases_seisme[i] = calloc(gameboard.size, sizeof(int));
    }


    /*AFFICHAGE D'UN MESSAGE DE DEBUT DE PARTIE*/
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    printf(ANSI_COLOR_RED);
    printf("   === Début de la partie ===");
    printf(ANSI_COLOR_RESET);
    printf("\n\n");




    /*-----------------------------------------------------------------------*/
    /*                        BOUCLE PRINCIPALE DU JEU                       */
    /*-----------------------------------------------------------------------*/

    /*Cette boucle a lieu tant que l'utilisateur ne demande pas de quitter*/
    while(game_continue) {
        /*Si la partie doit être remise à zéro (nouvelle partie)*/
        if(game_reset) {
            /*Affichage d'un message pour prévenir de la nouvelle partie*/
            printf(ANSI_COLOR_MAGENTA);
            printf("Partie remise à zéro.\n");
            printf("Nouvelle partie avec les mêmes paramètres.");
            printf(ANSI_COLOR_RESET);
            printf("\n\n");

            /*On vide le plateau de jeu*/
            for(i = 0; i < gameboard.size; i++)
                for(j = 0; j < gameboard.size; j++)
                    /*@loop variant: la longueur de la liste est diminuée de 1*/
                    while(!est_vide(gameboard.t[i][j]))
                        pop(&(gameboard.t[i][j]));

            /*On réinitialise les variables du programme*/
            victoire = -1;
            game_ended = 0;
            game_reset = 0;
            player = 1;
            x = 0;
            y = 0;
        }

        /*AFFICHAGES*/

        /*Affichage des paramètres de jeu à chaque tour*/
        affiche_parametres(gameboard, mode, seisme);

        /*Affichage du tableau de jeu*/
        printf("\n\n");
        affiche_gameboard(gameboard, x, y, cases_seisme, seisme);
        printf("\n\n\n");

        /*Affichage du joueur actuel à chaque tour*/
        printf(ANSI_COLOR_YELLOW);
        if(!game_ended)
            printf("C'est au tour du joueur %c.", JETONS[player]);
        /*Affichage du joueur qui a gagné la partie*/
        else {
            /*Affichage de la victoire avant séisme (pas d'égalité possible)*/
            if(victoire == 11 || victoire == 12)
                printf("Le joueur %c a gagné avant séisme.", JETONS[victoire-11]);

            /*Affichage de la victoire après séisme (égalité possible)*/
            if(victoire == 21 || victoire == 22)
                printf("Le joueur %c a gagné après séisme.", JETONS[victoire-21]);
            if(victoire == 23)
                printf("Egalité entre les deux joueurs après séisme.");

            /*Affichage de la victoire sans séisme actif (pas d'égalité possible)*/
            if(victoire == 1 || victoire == 2)
                printf("Le joueur %c a gagné.", JETONS[victoire-1]);
        }
        printf(ANSI_COLOR_RESET);
        printf("\n\n");

        /*Rappel des actions possibles à chaque tour*/
        printf(ANSI_COLOR_CYAN);
        if(!game_ended) {
            printf("    Poser un jeton : p\n");
            printf("  Retirer un jeton : r\n");
        }
        else {
            printf("\n");
            printf("           Rejouer : a\n");
        }
        printf("       Déplacement : i, j, k, l\n");
        printf("    Quitter le jeu : q");
        printf(ANSI_COLOR_RESET);
        printf("\n\n");


        /*Affichage des éventuelles erreurs au précédent tour*/
        if(depile_impossible || deplacement_impossible || action_invalide)
            printf(ANSI_COLOR_RED);

        if(depile_impossible) {
            printf("Impossible de retirer un jeton : cette pile est vide.");
            depile_impossible = 0;
        }
        else if(deplacement_impossible) {
            printf("Impossible de se déplacer hors du plateau de jeu.");
            deplacement_impossible = 0;
        }
        else if(action_invalide) {
            printf("Votre action '%c' est invalide.", c);
            action_invalide = 0;
        }

        if(depile_impossible || deplacement_impossible || action_invalide)
            printf(ANSI_COLOR_RESET);

        printf("\n");


        /*Récupération de l'action du joueur actuel*/
        printf(ANSI_COLOR_YELLOW);
        if(!game_ended)
            printf("Joueur %c, quelle est votre action ? ", JETONS[player]);
        else
            printf("Quelle est votre action ? ");
        fflush(stdout);
        scanf("%s", user_input);
        c = tolower(user_input[0]);
        printf(ANSI_COLOR_RESET);

        printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

        /*Si l'action du joueur est invalide*/
        if(!dans_tableau(TOUCHES, 8, c)
        || (c == 'a' && !game_ended)
        || ((c == 'p' || c == 'r') && game_ended))
            action_invalide = 1;

        /*Action : déplacement sur le plateau de jeu*/
        if(c == 'i')  y -= 1;
        if(c == 'j')  x -= 1;
        if(c == 'k')  y += 1;
        if(c == 'l')  x += 1;

        /*Hors terrain : mise à jour d'une variable pour afficher une erreur*/
        if(x < 0 || x > gameboard.size - 1 || y < 0 || y > gameboard.size - 1)
            deplacement_impossible = 1;

        /*Restriction de l'espace de jeu : pas de hors terrain*/
        if(x <= 0)                     x = 0;
        if(x >= gameboard.size - 1)    x = gameboard.size - 1;
        if(y <= 0)                     y = 0;
        if(y >= gameboard.size - 1)    y = gameboard.size - 1;

        /*Action : empilement (seulement si la partie est en cours)*/
        if(c == 'p' && !game_ended) {
            empile(gameboard, x, y, player);
            player = (player+1)%2;
        }

        /*Action : dépilement (seulement si la partie est en cours)*/
        if(c == 'r' && !game_ended) {
            /*Cas où on peut effectivement dépiler la case sélectionnée*/
            if(!est_vide(gameboard.t[y][x])) {
                pop_matrix(gameboard, x, y);
                player = (player+1)%2;
            }
            /*Cas où on ne peut pas dépiler la case sélectionnée*/
            else
                depile_impossible = 1;
        }

        /*Action : nouvelle partie (seulement si la partie est finie)*/
        if(c == 'a' && game_ended) {
            game_reset = 1;
            continue; /*On quitte cette itération de boucle pour réinitialiser*/
        }

        /*Action : fin de partie*/
        if(c == 'q') {
            game_continue = 0;
            break; /*On quitte la boucle de jeu*/
        }


        /*Remise à zéro des cases séismes pour éviter des affichages parasites*/
        if(seisme && (c == 'p' || c == 'r') && !game_ended && !depile_impossible)
            for(i = 0; i < gameboard.size; i++)
                for(j = 0; j < gameboard.size; j++)
                    cases_seisme[i][j] = 0;


        if(seisme) {
            /*Vérification des conditions de victoire avant séisme
              La vérification n'a lieu que si on a posé ou retiré un jeton*/
            if((c == 'p' || c == 'r') && !game_ended && !depile_impossible)
                victoire = (*check_victoire)(gameboard) + 10;

            /*Mise à jour de la variable de fin de partie si victoire*/
            if(victoire == 11 || victoire == 12)
                game_ended = 1;
        }


        /*Application de l'effet séisme (si séisme actif)*/
        double p_effondrement;
        double p;
        int nb_chutes;
        if(seisme && (c == 'p' || c == 'r') && !game_ended && !depile_impossible) {
            for(i = 0; i < gameboard.size; i++) {
                for(j = 0; j < gameboard.size; j++) {
                    cases_seisme[i][j] = 0;

                    /*Si la hauteur de la case est nulle, pas de séisme*/
                    if(hauteur(gameboard.t[i][j]) < 1)
                        continue;

                    /*Calcul des probabilités*/
                    p = 1.0 * rand() / RAND_MAX;
                    p_effondrement = 1.0 - pow(2, - hauteur(gameboard.t[i][j]) / (2.0*gameboard.size));

                    /*Si la proba d'effondrement est vérifiée*/
                    if(p <= p_effondrement) {
                        /*On détermine un nombre de jetons qui chutent*/
                        nb_chutes = 1 + rand() % hauteur(gameboard.t[i][j]);
                        /*On marque cette case comme ayant subi un séisme*/
                        cases_seisme[i][j] = 1;
                        /*On supprime effectivement les jetons*/
                        for(k = 0; k < nb_chutes; k++)
                            pop(&(gameboard.t[i][j]));
                    }
                }
            }
        }

        /*Mise à jour de la hauteur maximale du tableau de jeu*/
        if(!game_ended)
            update_hauteur(&gameboard);


        /*Vérification des conditions de victoire après séisme
          Vérification des conditions de victoire si mode séisme non actif
          La vérification n'a lieu que si on a posé ou retiré un jeton*/
        if((c == 'p' || c == 'r') && !game_ended && !depile_impossible) {
            if(seisme)
                victoire = (*check_victoire)(gameboard) + 20;
            else
                victoire = (*check_victoire)(gameboard);
        }

        /*Mise à jour de la variable de fin de partie si victoire*/
        if(seisme && (victoire == 21 || victoire == 22 || victoire == 23))
            game_ended = 1;

        /*Mise à jour de la variable de fin de partie si victoire*/
        if(victoire == 1 || victoire == 2)
            game_ended = 1;
    }

    /*Affichage d'un message quand le jeu est quitté*/
    printf("\n\n");
    printf(ANSI_COLOR_MAGENTA);
    printf("Le jeu est désormais quitté.\nMerci d'avoir joué !");
    printf(ANSI_COLOR_RESET);
    printf("\n\n");

    /*Libération de la mémoire*/
    freeMatrix(gameboard);
    free(cases_seisme);

    return 1;
}

/*@requires: rien
  @assigns: rien
  @ensures: retourne 1 si c est dans le tableau et 0 sinon*/
int dans_tableau(char* tab, int size, char c) {
	int i;
	for(i = 0; i < size; i++)
		if(c == tab[i])
			return 1;
	return 0;
}

/*Empile le jeton du joueur dans le plateau à une position donnée*/
/*@requires: gameboard bien assignée
  @assigns: gameboard.t[y][x]
  @ensures: ajoute 1 à gameboard.t[y][x] si le joueur est O
            ajoute -1 à gameboard.t[y][x] si le joueur est X*/
void empile(matrix gameboard, int x, int y, int player) {
    if(player == 0) /*Si le joueur est O*/
        add_matrix(gameboard, x, y, 1);
    if(player == 1) /*Si le joueur est X*/
        add_matrix(gameboard, x, y, -1);
}
