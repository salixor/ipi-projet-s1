/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: display.h
*/

#ifndef HEADER_DISPLAY
#define HEADER_DISPLAY

#ifndef ANSI_COLOR_RESET
#define ANSI_COLOR_RED     "\x1b[91m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[95m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#endif

#include "matrix.h"

void affiche_parametres(matrix, int, int);
void affiche_gameboard(matrix, int, int, int**, int);
void affiche_ligne(int, int, int, int, int**, int);
void affiche_cote(int, int, int, int, int**, int);

void fail_allocation();

#endif
