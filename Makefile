# Auteur: Kévin Cocchi
# Projet : Morpion 3D

CC=gcc -Wall -Wextra -ansi -lm
all: morpion

lists.o: lists.c
matrix.o: matrix.c
victory.o: victory.c
display.o: display.c
main.o: main.c

morpion: lists.o matrix.o victory.o display.o main.o
	$(CC) -o $@ $^
