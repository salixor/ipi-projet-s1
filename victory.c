/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: victory.c
*/

#include "victory.h"

/*Fonction de vérification des conditions de victoire en vue du dessus*/
/*@requires: gameboard bien définie
  @assigns: rien
  @ensures: retourne 0 si aucun gagnant
			retourne 1 si O gagne
			retourne 2 si X gagne
			retourne 3 si égalité*/
int check_victoire_2D(matrix gameboard) {
	int i, j;

	int GS = gameboard.size;
	int sum_C = 0, sum_L = 0; /*Variables utiles à la sommation ligne/colonne*/
	int sum_D = 0, sum_AD = 0; /*Variables utiles à la sommation diagonale*/

	int victoire_O = 0;
	int victoire_X = 0;
	/*Cette première boucle somme sur les deux diagonales du carré*/
	for(i = 0; i < GS; i++) {
		sum_D += peek(gameboard.t[i][i], 0);
		sum_AD += peek(gameboard.t[i][GS-1-i], 0);
	}

	/*On met à jour les variables de victoire*/
	if(sum_D == GS || sum_AD == GS)     victoire_O = 1;
	if(sum_D == -GS || sum_AD == -GS)   victoire_X = 2;

	/*Cette seconde boucle somme sur les lignes et colonnes*/
	for(i = 0; i < GS; i++) {
		sum_C = 0;
		sum_L = 0;
		for(j = 0; j < GS; j++) {
			sum_C += peek(gameboard.t[i][j], 0);
			sum_L += peek(gameboard.t[j][i], 0);
		}

		/*On met à jour les variables de victoire*/
		if(sum_L == GS || sum_C == GS)    victoire_O = 1;
		if(sum_L == -GS || sum_C == -GS)  victoire_X = 2;
	}

	return victoire_O + victoire_X;
}

/*Fonction de vérification des conditions de victoire en 3D*/
/*@requires: gameboard bien définie
  @assigns: rien
  @ensures: retourne 0 si aucun gagnant
			retourne 1 si O gagne
			retourne 2 si X gagne
			retourne 3 si égalité*/
int check_victoire_3D(matrix gameboard) {
	int i, j, k, l;
	int GS = gameboard.size;
	int GH = gameboard.hauteur_maxi;

	/*Variables utiles aux sommations par étage*/
	int sum_C = 0, sum_L = 0;
	int sum_D = 0, sum_AD = 0;

	int sum_H = 0; /*Variable utilise à la sommation dans une pile*/

	/*Variables utiles aux sommations en escaliers*/
	int sum_C1 = 0, sum_C2 = 0;
	int sum_L1 = 0, sum_L2 = 0;
	int sum_D1 = 0, sum_D2 = 0;
	int sum_AD1 = 0, sum_AD2 = 0;

	int victoire_O = 0;
	int victoire_X = 0;


	/*Boucle sur tous les étages pour vérifier en 2D à chaque étage*/
	for(k = 0; k <= GH; k++) {
		/*Vérification sur les diagonales de chaque étage*/
		sum_D = 0;
		sum_AD = 0;

		for(i = 0; i < GS; i++) {
			sum_D += peek(gameboard.t[i][i], hauteur(gameboard.t[i][i]) - k);
			sum_AD += peek(gameboard.t[i][GS-1-i], hauteur(gameboard.t[i][GS-1-i]) - k);
		}

		/*On met à jour les variables de victoire*/
		if(sum_D == GS || sum_AD == GS)     victoire_O = 1;
		if(sum_D == -GS || sum_AD == -GS)   victoire_X = 2;


		/*Vérification sur les lignes et colonnes de chaque étage*/
		for(i = 0; i < GS; i++) {
			sum_C = 0;
			sum_L = 0;
			for(j = 0; j < GS; j++) {
				sum_C += peek(gameboard.t[i][j], hauteur(gameboard.t[i][j]) - k);
				sum_L += peek(gameboard.t[j][i], hauteur(gameboard.t[j][i]) - k);
			}

			/*On met à jour les variables de victoire*/
			if(sum_L == GS || sum_C == GS)     victoire_O = 1;
			if(sum_L == -GS || sum_C == -GS)   victoire_X = 2;
		}
	}

	/*Boucle sur le nombre de cas où une victoire en escalier / dans une pile
		est possible (par exemple, jeu de taille 3 et pile de 5 => 2 boucles)*/
	for(k = 0; k <= GH - GS; k++) {
		/*Vérification sur les diagonales montantes et descendantes*/
		sum_D1 = 0;
		sum_D2 = 0;
		sum_AD1 = 0;
		sum_AD2 = 0;

		for(i = 0; i < GS; i++) {
			/*Diagonale ascendante ; Diagonale directe*/
			sum_D1 += peek(gameboard.t[i][i], hauteur(gameboard.t[i][i]) - 1 - k - i);
			/*Diagonale ascendante ; Diagonale indirecte*/
			sum_AD1 += peek(gameboard.t[i][GS-1-i], hauteur(gameboard.t[i][GS-1-i]) - 1 - k - i);
			/*Diagonale descendante ; Diagonale directe*/
			sum_D2 += peek(gameboard.t[GS-1-i][GS-1-i], hauteur(gameboard.t[GS-1-i][GS-1-i]) - 1 - k - i);
			/*Diagonale descendante ; Diagonale indirecte*/
			sum_AD2 += peek(gameboard.t[GS-1-i][i], hauteur(gameboard.t[GS-1-i][i]) - 1 - k - i);
		}

		/*On met à jour les variables de victoire*/
		if(sum_D1 == GS || sum_AD1 == GS || sum_D2 == GS || sum_AD2 == GS)
			victoire_O = 1;
		if(sum_D1 == -GS || sum_AD1 == -GS || sum_D2 == -GS || sum_AD2 == -GS)
			victoire_X = 2;


		/*Vérification sur les escaliers montants et descendants*/
		for(i = 0; i < GS; i++) {
			sum_C1 = 0;
			sum_C2 = 0;
			sum_L1 = 0;
			sum_L2 = 0;

			for(j = 0; j < GS; j++) {
				/*Escalier montant ; Sur une ligne*/
				sum_C1 += peek(gameboard.t[i][j], hauteur(gameboard.t[i][j]) - 1 - k - j);
				/*Escalier descendant ; Sur une ligne*/
				sum_C2 += peek(gameboard.t[i][GS-1-j], hauteur(gameboard.t[i][GS-1-j]) - 1 - k - j);
				/*Escalier montant ; Sur une colonne*/
				sum_L1 += peek(gameboard.t[j][i], hauteur(gameboard.t[j][i]) - 1 - k - j);
				/*Escalier descendant ; Sur une colonne*/
				sum_L2 += peek(gameboard.t[GS-1-j][i], hauteur(gameboard.t[GS-1-j][i]) - 1 - k - j);
			}

			/*On met à jour les variables de victoire*/
			if(sum_L1 == GS || sum_C1 == GS || sum_L2 == GS || sum_C2 == GS)
				victoire_O = 1;
			if(sum_L1 == -GS || sum_C1 == -GS || sum_L2 == -GS || sum_C2 == -GS)
				victoire_X = 2;
		}


		/*Vérification sur les empilements dans une pile*/
		for(i = 0; i < GS; i++) {
			for(j = 0; j < GS; j++) {
				sum_H = 0;

				/*Si la hauteur de la pile n'est pas suffisante, on passe*/
				if(hauteur(gameboard.t[i][j]) < GS)
					continue;

				for(l = 0; l < GS; l++)
					sum_H += peek(gameboard.t[i][j], k + l);

				/*On met à jour les variables de victoire*/
				if(sum_H == GS)    victoire_O = 1;
				if(sum_H == -GS)   victoire_X = 2;
			}
		}
	}

	return victoire_O + victoire_X;
}
