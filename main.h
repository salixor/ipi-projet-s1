/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: main.h
*/

#ifndef HEADER_MAIN
#define HEADER_MAIN

#ifndef ANSI_COLOR_RESET
#define ANSI_COLOR_RED     "\x1b[91m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[95m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#endif

int (*check_victoire)(matrix);
int dans_tableau(char*, int, char);
void empile(matrix, int, int, int);

#endif
