/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: victory.h
*/

#ifndef HEADER_VICTORY
#define HEADER_VICTORY

#include "matrix.h"

int check_victoire_2D(matrix);
int check_victoire_3D(matrix);

#endif
