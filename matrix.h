/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: matrix.h
*/

#ifndef HEADER_MATRIX
#define HEADER_MATRIX

#include "lists.h"

typedef struct matrix {
    int size;
    int hauteur_maxi;
    list **t;
} matrix;

matrix makeMatrix(int);
void freeMatrix(matrix);

void update_hauteur(matrix *gameboard);
void add_matrix(matrix, int, int, int);
int pop_matrix(matrix, int, int);

#endif
