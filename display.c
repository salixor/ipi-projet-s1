/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: display.c
*/

#include "display.h"

#include <stdio.h>


/*@requires: rien
  @assigns: rien
  @ensures: affiche les paramètres de la partie*/
void affiche_parametres(matrix gameboard, int mode, int seisme) {
    printf(" Taille du plateau : %d*%d.\n", gameboard.size, gameboard.size);
    if(mode == 2)
        printf("          Variante : Vue du dessus.\n");
    if(mode == 3)
        printf("          Variante : 3D.\n");
    if(seisme)
        printf("     Option séisme : Active.\n");
    else
        printf("     Option séisme : Inactive.\n");
}

/*@requires: rien
  @assigns: rien
  @ensures: affiche le plateau de jeu et de la pile sélectionnée*/
void affiche_gameboard(matrix gameboard, int x, int y, int** cases_seisme, int seisme) {
    int i, j;

    /*Affichage de la pile à droite (partie supérieure si besoin)*/
    for(i = 0; i < hauteur(gameboard.t[y][x]) - gameboard.size*3; i++) {
        printf("   ");
        /*Ajout du blanc pour être à droite (plateau absent)*/
        for(j = 0; j < gameboard.size; j++)
            printf("   ");

        /*Affichage des jetons*/
        if(peek(gameboard.t[y][x], i) == -1)
            printf("          X");
        else if(peek(gameboard.t[y][x], i) == 1)
            printf("          O");
        else
            printf("           ");
        printf("\n");
    }

    /*Affichage du plateau et de la pile sélectionnée (partie inférieure)*/
    for(i = 0; i < gameboard.size*3; i++) {
        /*Affichage supérieur d'une ligne de la grille*/
        if(i%3 == 0)
            affiche_ligne(gameboard.size, x, y, i, cases_seisme, seisme);

        /*Affichage au milieu d'une ligne de la grille*/
        if(i%3 == 1) {
            printf("   ");
            for(j = 0; j < gameboard.size; j++) {
                /*Affichage à gauche de la case*/
                affiche_cote(x, y, i, j, cases_seisme, seisme);

                /*Affichage au milieu de la case*/
                if(peek(gameboard.t[i/3][j], 0) == -1)      printf("X");
                else if(peek(gameboard.t[i/3][j], 0) == 1)  printf("O");
                else                                        printf("•");

                /*Affichage à droite de la case*/
                affiche_cote(x, y, i, j, cases_seisme, seisme);
            }
        }

        /*Affichage inférieur d'une ligne de la grille*/
        if(i%3 == 2)
            affiche_ligne(gameboard.size, x, y, i, cases_seisme, seisme);


        /*Affichage de la pile à droite*/
        if(peek(gameboard.t[y][x], hauteur(gameboard.t[y][x]) - gameboard.size*3 + i) == -1)
            printf("          X");
        else if(peek(gameboard.t[y][x], hauteur(gameboard.t[y][x]) - gameboard.size*3 + i) == 1)
            printf("          O");
        else
            printf("           ");
        printf("\n");
    }

    /*Affichage en dessous du plateau de jeu et de la pile sélectionnée*/
    printf("   ");
    for(j = 0; j < gameboard.size; j++)
        printf("---");
    printf("         ---");
}

/*@requires: rien
  @assigns: rien
  @ensures: affiche les lignes supérieures et inférieures du plateau de jeu*/
void affiche_ligne(int gs, int x, int y, int i, int** cases_seisme, int seisme) {
    int k;

    printf("   ");
    for(k = 0; k < gs; k++) {
        if(seisme) {
            if(i/3 == y && k == x && cases_seisme[i/3][k])   printf("~+~");
            else if(i/3 == y && k == x)                      printf(" + ");
            else if(cases_seisme[i/3][k])                    printf("~~~");
            else                                             printf("   ");
        }
        else {
            if(i/3 == y && k == x)                      printf(" + ");
            else                                        printf("   ");
        }
    }
}

/*@requires: rien
  @assigns: rien
  @ensures: affiche les côtés du jeton dans le plateau de jeu*/
void affiche_cote(int x, int y, int i, int j, int** cases_seisme, int seisme) {
    if(seisme) {
        if(i/3 == y && j == x)         printf("+");
        else if(cases_seisme[i/3][j])  printf("~");
        else                           printf(" ");
    }
    else {
        if(i/3 == y && j == x)         printf("+");
        else                           printf(" ");
    }
}

/*@requires: rien
  @assigns: rien
  @ensures: affiche un message d'erreur si l'allocation échoue*/
void fail_allocation() {
    printf(ANSI_COLOR_RED);
    printf("La taille du plateau de jeu est trop grande.\n");
    printf("Il a été impossible d'allouer la mémoire nécessaire.");
    printf(ANSI_COLOR_RESET);
    printf("\n");
}
