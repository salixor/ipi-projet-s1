/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: lists.c
*/

#include <stdlib.h>
#include "lists.h"

/*Test de vacuité d'une liste chaînée*/
/*@requires: rien
  @assigns: rien
  @ensures: retourne 1 si la liste est vide et 0 sinon*/
int est_vide(list l) {
    return ( NULL == l );
}

/*Libère la mémoire allouée pour une liste*/
/*@requires: rien
  @assigns: *pl
  @ensures: supprime la liste pl de la mémoire*/
void make_empty(list *pl) {
    list tmp;
    /*@loop variant: la longuer de la liste est diminuée de 1*/
    while((*pl) != NULL) {
        tmp = (*pl);
        (*pl) = (*pl)->next;
        free(tmp);
    }
    *pl = NULL;
}

/*Ajout par effet d'un élement à une liste chaînée*/
/*@requires: rien
  @assigns: *pl
  @ensures: ajoute la valeur v à la liste pl*/
void add(int v, list *pl) {
    list res = (list) malloc(sizeof(struct maillon));
    res->val = v;
    res->next = *pl;
    *pl = res;
}

/*Suppression de l'élément en tête de la liste chaînée*/
/*@requires: rien
  @assigns: *pl
  @ensures: supprime la valeur en tête de la liste et la retourne ou retourne 0
            si la liste est vide*/
int pop(list *pl) {
    int res = 0;
    if(!est_vide(*pl)) {
        list tmp = *pl;
        res = (*pl)->val;
        *pl = (*pl)->next;
        free(tmp);
    }
    return res;
}

/*Accès à la valeur de la liste à une profondeur donnée*/
/*@requires: rien
  @assigns: rien
  @ensures: retourne la valeur de la liste à la profondeur donnée si possible
            et 0 sinon*/
int peek(list l, int profondeur) {
    int i;
    if(profondeur >= 0) {
        for(i = 0; i < profondeur; i++)
            if(!est_vide(l))
                l = l->next;
        if(!est_vide(l))
            return l->val;
    }
    return 0;
}

/*Détermine la longueur d'une liste*/
/*@requires: rien
  @assigns: rien
  @ensures: retourne la longueur de la liste l*/
int hauteur(list l) {
    int h = 0;
    /*@loop variant: h est incrémenté de 1*/
    while(l != NULL) {
        h++;
        l = l->next;
    }
    return h;
}
