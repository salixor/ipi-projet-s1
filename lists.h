/*
* @Auteur: Kévin Cocchi
* @Projet: Morpion 3D
* @Fichier: lists.h
*/

#ifndef HEADER_LISTS
#define HEADER_LISTS

typedef struct maillon* list;
struct maillon {
    int val;
    list next;
};

int est_vide(list);
void make_empty(list*);
void add(int, list*);
int pop(list*);
int peek(list, int);
int hauteur(list);

#endif
